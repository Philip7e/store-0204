

window.onload = function () {
	// адаптивне бургер меню
	document.querySelector(".burger").addEventListener("click", function () {
		this.classList.toggle("active");
		document.querySelector(".burger-container").classList.toggle("open");
	});

	// Анімація корзини при додаванні товару
	if (document.querySelector(".basket-counter").textContent > 0) {
		document.querySelector(".basket").classList.add("fa-bounce");
	} else {
		document.querySelector(".basket").classList.remove("fa-bounce");
	}

	//випадаюче філтьр-меню
	document.querySelectorAll(".filter-name").forEach((el) => {
		el.addEventListener("click", () => {
			el.children[0].classList.toggle("fa-rotate-180");
			el.nextElementSibling.classList.toggle("hide");
		});
	});

	// відкрити модальне вікно
	document.querySelectorAll(".show-products-card").forEach((el) => {
		el.addEventListener("click", (evt) => {
			if ((evt.target.parentElement.classList == "add-to-cart")) {return}
			document.querySelector(".modal").classList.toggle("hide");
		});
	});

	// закрити модальне вікно
	try {
		document.querySelector(".close-modal").addEventListener("click", () => {
			document.querySelector(".modal").classList.toggle("hide");
		});	
	} catch (e) {
		if (document.location.pathname.includes("/catalog/")) {
			new Error(e);
			//console.error(e);
		}
	}
	
	//відкрити корзину
	document.querySelector(".basket-box").addEventListener("click", () => {
		document.location.pathname="/cart_page"
	})
	
};